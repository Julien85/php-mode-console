<?php 

/* le script récupère deux saisies utilisateurs, 
il vérifie qu'il ne s'agit pas deux fois de la même saisie,
il vérifie que les mots sont des anagrammes, 
et indique la réponse à l'utilisateur. 
 */

echo "Veuillez saisir un mot : ";
$mot1 = trim(fgets(STDIN));
$mot1 = strtolower($mot1);

echo "Veuillez saisir un autre mot : ";
$mot2 = trim(fgets(STDIN));
$mot2 = strtolower($mot2);

$array1 = str_split($mot1);
    $ar1 = sort($array1);
$array2 = str_split($mot2);
    $ar2 = sort($array2);

//var_dump($array1, $array2);

if($array1 == $array2){
    echo "Ce sont bien des anagrammes, bravo!\n";
}else{
    echo "Ces mots ne sont pas des anagrammes!\n";
}