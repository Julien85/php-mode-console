<?php

####
####
####
####

####
#  #
#  #
####

#   #
 # #
  #
 # #
#   #

#
##
# #
#  #
#####

  #
 # #
#   #
 # #
  #

# # #
 # #
# # #
 # #
# # #

//demander à l'utilisateur la hauteur de la figure
//demander à l'utilisateur la largeur de la figure
//demander à l'utilisateur le caractere utilisé pour dessiner
//demander à l'utilisateur quelle figure dessiner (valeurs possible de 1 à 6)
//dessiner la figure

echo "Veuillez saisir la hauteur pour la figure : ";
$hauteur = intval(fgets(STDIN));
echo "Veuillez saisir la largeur pour la figure : ";
$largeur = intval(fgets(STDIN));
echo "Veuillez saisir le caractere pour dessiner : ";
$caractere = trim(fgets(STDIN));
echo "Veuillez saisir la figure à dessiner : ";
$dessin = intval(fgets(STDIN));

/*do{
    echo "Veuillez saisir la hauteur pour la figure : ";
$hauteur = intval(fgets(STDIN));
}
*/


switch ($dessin) {
    case 1:
        //1ere figure
        echo"---------------\n";
        for ($x=0; $x < $hauteur; $x++) { 
            for ($y=0; $y < $largeur; $y++) { 
                echo $caractere;
            }echo " \n";
        }
    break;

    case 2:
        // 2e figure
        echo"---------------\n";
        for ($x=0; $x < $hauteur; $x++) { 
            for ($y=0; $y < $largeur; $y++) { 
                if($x == 0 || $x == ($hauteur-1) || $y == 0 || $y == ($largeur-1)){
                    echo $caractere;
                }else{
                    echo " ";
                }
            }echo " \n";
        }
    break; 

    case 3:
        //3e figure
        echo"---------------\n";
        for ($x=0; $x < $hauteur; $x++) { 
            for ($y=0; $y < $largeur; $y++) { 
                if($x == $y || $y == ($hauteur-1)-$x){
                    echo $caractere;
                }else{
                    echo " ";
                }
            }echo " \n";
        }
    break;

    case 4:
        //4e figure
        echo"---------------\n";
        for ($x=0; $x < $hauteur; $x++) { 
            for ($y=0; $y < $largeur; $y++) { 
                if($x == $y || $y == 0 || $x == ($hauteur-1)){
                    echo $caractere;
                }else{
                    echo " ";
                }
            }echo " \n";
        }
    break;

    case 5:
        //5e figure
        echo"---------------\n";
        for ($x=0; $x < $hauteur; $x++) { 
            for ($y=0; $y < $largeur; $y++) { 
                if($y == $x+($largeur-1)/2 
                || $y == (($largeur-1)/2 - $x) 
                || $x == $y+($hauteur-1)/2
                || $x == $hauteur-1 -$y +($largeur -1)/2){
                    echo $caractere;
                }else{
                    echo " ";
                }
            }echo " \n";
        }
    break;

    case 6:
        //6e figure
        echo"---------------\n";
        for ($x=0; $x < $hauteur; $x++) { 
            for ($y=0; $y < $largeur; $y++) { 
                if(($x+$y)%2 == 0){
                    echo $caractere;
                }else{
                    echo " ";
                }
            }echo " \n";
        }
    break;

default : 
    echo "Veuillez taper un chiffre entre 1 et 6 \n";
}


?>