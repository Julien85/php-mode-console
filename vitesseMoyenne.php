<?php

/**
 * Ecrire un algorithme qui calcul la vitesse moyenne d'un déplacement
 * 1/ Demander à l'utilisateur de saisir la distance parcourue (en km)
 * 2/ Demander à l'utilisateur de saisir le temps pour effectuer le parcours (en mn)
 * 3/ Afficher la vitesse moyenne du déplacement en km/h
 */
echo "Veuillez saisir la distance parcourue (en km) : ";
$i = intval(fgets(STDIN));

echo "Veuillez saisir le temps pour effectuer le parcours (en mn) : ";
$j = intval(fgets(STDIN));

echo "La vitesse moyenne sur le parcours est de : ".round($i/$j*60)." km/h \n";

?>
