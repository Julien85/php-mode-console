<?php

 $array = [];
do {
    echo "Que souhaitez vous faire? (aide, tache, liste, supprimer, quitter)\n";
    $action = trim(fgets(STDIN));
    if($action == "aide" || $action == "a"){
        echo "Listes des commandes possibles : \n 
    aide/a        afficher les commandes possibles \r
    tache/t       créer une nouvelle tache \r
    liste/l       afficher la liste des tâches en cours \r
    supprimer/s   supprimer une tâche existante \r
    quitter/q     arrêter l'execution du script \r";
    }elseif($action == "tache" || $action == "t"){
        echo "Quel est le nom de votre tache?\n";
        $tache = trim(fgets(STDIN));
        $array[] = $tache;
        //var_dump($array);
    }elseif($action == "liste" || $action == "l"){
        if(count($array) == 0){
            echo "Pas de tâches prévues!\n";
        }else{
            echo "Listes des tâches en cours : \n";
            foreach ($array as $key => $value) {
            $tt=$key+1;
            echo "- $value" ." "."(".$tt.")"."\n";
            }
        }
    }elseif($action == "supprimer" || $action == "s"){
        if(count($array) == 0){
            echo "Pas de tâches à supprimer!\n";
        }else{
            do{
            echo "Quelle tache souhaitez vous supprimer? (saisir son id) \n";
                foreach ($array as $key => $value) {
                $tt=$key+1;    
                echo "- $value" ." "."(".$tt.")"."\n";
                }
            $sup = intval(fgets(STDIN));
            }while(!array_key_exists($sup-1, $array));
            unset($array[$sup-1]);
            //array_slice($array, $sup);
        }
    }
}while ($action != "quitter" && $action != "q");



 
